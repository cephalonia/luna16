from functools import lru_cache
from operator import attrgetter
import copy
import SimpleITK
import torch
import argparse
import collections
import csv
import logging
import glob
import os
import numpy as np
import sys
import math

# XYZ coordinate systems.
XYZ = collections.namedtuple("XYZ", ["x", "y", "z"])
IRC = collections.namedtuple("IRC", ["i", "r", "c"])

Candidate = collections.namedtuple(
    "Candidate", ["series_id", "center_xyz", "diameter_mm", "is_nodule"])


def CreateArgumentParser():
    argument_parser = argparse.ArgumentParser(
        prog="luna", description="luna trainer")

    argument_parser.add_argument("-b", "--batch-size",
                                 action="store",
                                 type=int,
                                 default=1,
                                 help="Batch size"
                                 )

    argument_parser.add_argument("-e", "--epoch",
                                 action="store",
                                 type=int,
                                 default=1,
                                 help="Batch size"
                                 )

    argument_parser.add_argument("-o", "--optimizer",
                                 action="store",
                                 type=str,
                                 default="sgd",
                                 choices=["sgd", "adam"],
                                 help="Optimizer.")

    argument_parser.add_argument("-lr", "--learning-rate",
                                 action="store",
                                 type=float,
                                 default=1e-2,
                                 help="Learning rate.")

    argument_parser.add_argument("--momentum",
                                 action="store",
                                 type=float,
                                 default=5e-1,
                                 help="Learning rate.")

    argument_parser.add_argument("-lf", "--loss-function",
                                 action="store",
                                 type=str,
                                 default="cross-entropy",
                                 choices=["cross-entropy"],
                                 help="Loss function.")

    argument_parser.add_argument("--cuda",
                                 action="store_true",
                                 default=False,
                                 help="Enbale traning on CUDA devices."
                                 )

    argument_parser.add_argument("--luna-data-dir",
                                 action="store",
                                 type=str,
                                 default="",
                                 help="Data directory of LUNA16"
                                 )

    argument_parser.add_argument("--save-model",
                                 action="store_true",
                                 default=False,
                                 help="Save model to disk.")
    return argument_parser


class Ct:
    def __init__(self, series_id, ct_image) -> None:
        self.series_id_ = series_id
        _ = np.array(SimpleITK.GetArrayFromImage(ct_image), dtype=np.float32)
        self.data_ = _.clip(-1000, 1000, _)
        self.origin_ = np.array(ct_image.GetOrigin())
        self.spacing_ = np.array(ct_image.GetSpacing())
        self.direction_ = np.array(ct_image.GetDirection()).reshape(3, 3)

    def TransformXYZ2IRC(self, xyz: XYZ):
        c, r, i = np.round(((xyz - self.origin_) @
                            np.linalg.inv(self.direction_))/self.spacing_)
        return IRC(i, r, c)

    def TransformIRC2XYZ(self, irc: IRC):
        cri = np.array(irc)[::-1]
        xyz = (self.direction_ @ (cri * self.spacing_)) + self.origin_
        return XYZ(*xyz)

    def GetCandidateBlock(self, block_center_xyz, block_ranges):
        block_center_irc = self.TransformXYZ2IRC(block_center_xyz)
        slices = list()
        for center, ranges, bound in zip(block_center_irc, block_ranges, self.data_.shape):
            if center < 0 or center >= bound:
                raise RuntimeError(
                    "Corrupt data, block center is out of bound.")
            start = int(round(center - ranges/2))
            end = int(start + ranges)
            if start < 0:
                start = 0
                end = int(ranges)
            if end > bound:
                end = bound
                start = int(bound - ranges)
            slices.append(slice(start, end))

        return (block_center_irc, self.data_[tuple(slices)])


class LunaDataset(torch.utils.data.Dataset):
    def __init__(self, is_eval_set: False, eval_stride=10) -> None:
        super().__init__()

        self.candidate_list_ = copy.copy(LunaContext.Get().GetCandidateList())
        if is_eval_set:
            assert eval_stride > 0
            self.candidate_list_ = self.candidate_list_[::eval_stride]
            assert self.candidate_list_
        elif eval_stride > 0:
            del self.candidate_list_[::eval_stride]
            assert self.candidate_list_

    def __len__(self):
        return len(self.candidate_list_)

    def __getitem__(self, index):
        luna_context = LunaContext.Get()
        candidate = self.candidate_list_[index]
        block_ranges_irc = [32, 48, 48]
        center_irc, data_block = luna_context.GetCtCandidateBlock(
            candidate.series_id, candidate.center_xyz, block_ranges_irc)

        data_block_tensor = torch.from_numpy(data_block)
        data_block_tensor = data_block_tensor.to(torch.float32)
        data_block_tensor = data_block_tensor.unsqueeze(0)

        positive_t = torch.tensor([not candidate.is_nodule,
                                   candidate.is_nodule], dtype=torch.float32)
        return (candidate.series_id, data_block_tensor, positive_t, torch.tensor(center_irc, dtype=torch.float32))


class LunaContext:
    instance = None

    @classmethod
    def Initialize(cls, args: dict):
        if cls.instance is None:
            luna_data_dir = args.get("luna_data_dir")

            annotation_csv_path = os.path.join(
                luna_data_dir, "annotations.csv")
            if not os.path.exists(annotation_csv_path):
                raise FileExistsError("annotations.csv not found!")

            candidate_csv_path = os.path.join(luna_data_dir, "candidates.csv")
            if not os.path.exists(candidate_csv_path):
                raise FileExistsError("candidates.csv not found!")

            file_list = glob.glob(os.path.join(
                luna_data_dir, "subset*", "*.mhd"))
            series_id = [os.path.split(p)[-1][:-4] for p in file_list]
            series_id_path_dict = {sid: fpath
                                   for (sid, fpath) in zip(series_id, file_list)}

            args.setdefault(
                "annotation_csv_path", annotation_csv_path)
            args.setdefault(
                "candidate_csv_path", candidate_csv_path)
            args.setdefault(
                "series_id_path_dict", series_id_path_dict)

            cls.instance = LunaContext(args)
        else:
            raise RuntimeError("Instance has already been initialized.")

    @classmethod
    def Get(cls):
        if cls.instance is None:
            raise RuntimeError("Call LunaContext.Initialize first!")
        return cls.instance

    def __init__(self, args: dict) -> None:
        self.args_ = args
        self.annotation_csv_path_ = self.args_.get("annotation_csv_path")
        self.candidate_csv_path_ = self.args_.get("candidate_csv_path")
        self.series_id_path_dict_ = self.args_.get("series_id_path_dict")
        # internal.
        self.cached_ct_ = None

    @lru_cache(maxsize=2)
    def GetCandidateList(self):
        diameter_dict = dict()

        # processing annotations.csv
        with open(self.annotation_csv_path_, 'r') as f:
            for row in list(csv.reader(f))[1:]:
                series_id = row[0]
                annotation_center_xyz = tuple([float(x) for x in row[1:4]])
                annotation_diameter_mm = float(row[4])

                diameter_dict.setdefault(series_id, []).append(
                    (annotation_center_xyz, annotation_diameter_mm))

        candidate_list = []
        # processing candidates.csv
        with open(self.candidate_csv_path_, "r") as f:
            for row in list(csv.reader(f))[1:]:
                series_id = row[0]
                is_nodule = bool(int(row[4]))
                candidate_center_xyz = tuple(float(x) for x in row[1:4])
                candidate_diameter_mm = 0.0

                for annotation_tup in diameter_dict.get(series_id, []):
                    annotation_center_xyz, annotation_diameter_mm = annotation_tup
                    for i in range(3):
                        diff_mm = abs(
                            candidate_center_xyz[i] - annotation_center_xyz[i])
                        if diff_mm > annotation_diameter_mm / 4.0:
                            break
                    else:
                        candidate_diameter_mm = annotation_diameter_mm
                        # why break? because there's only one candidate per row.
                        break
                candidate_list.append(Candidate(series_id,
                                                candidate_center_xyz,
                                                candidate_diameter_mm,
                                                is_nodule))
        candidate_list.sort(key=attrgetter("series_id"))
        return candidate_list

    def GetCt(self, series_id):
        if self.cached_ct_ is None or self.cached_ct_.series_id_ != series_id:
            path = self.series_id_path_dict_.get(series_id)
            if path is None:
                raise RuntimeError(
                    "series_id {} has no corresponding file on disk.".format(series_id))
            self.cached_ct_ = Ct(series_id, SimpleITK.ReadImage(path))
        return self.cached_ct_

    def GetCtCandidateBlock(self, series_id, block_center_xyz, block_ranges_irc):
        ct = self.GetCt(series_id)
        block = ct.GetCandidateBlock(block_center_xyz, block_ranges_irc)
        return block


class LunaModel(torch.nn.Module):
    def __init__(self, in_channels=1, conv_channels=8):
        super().__init__()

        # Batch norm layer.
        self.tail_batchnorm = torch.nn.BatchNorm3d(1)

        self.block1 = LunaBlock(in_channels, conv_channels)
        self.block2 = LunaBlock(conv_channels, conv_channels * 2)
        self.block3 = LunaBlock(conv_channels * 2, conv_channels * 4)
        self.block4 = LunaBlock(conv_channels * 4, conv_channels * 8)

        self.head_linear = torch.nn.Linear(1152, 2)

        self.head_softmax = torch.nn.Softmax(dim=1)

        self._init_weights()

    def forward(self, input_batch):
        bn_output = self.tail_batchnorm(input_batch)

        block_out = self.block1(bn_output)
        block_out = self.block2(block_out)
        block_out = self.block3(block_out)
        block_out = self.block4(block_out)

        conv_flat = block_out.view(block_out.size(0), -1,)
        linear_output = self.head_linear(conv_flat)

        return linear_output, self.head_softmax(linear_output)

    # see also https://github.com/pytorch/pytorch/issues/18182
    def _init_weights(self):
        for m in self.modules():
            if type(m) in {torch.nn.Linear,
                           torch.nn.Conv3d,
                           torch.nn.Conv2d,
                           torch.nn.ConvTranspose2d,
                           torch.nn.ConvTranspose3d, }:
                torch.nn.init.kaiming_normal_(
                    m.weight.data, a=0, mode='fan_out', nonlinearity='relu',)
                if m.bias is not None:
                    fan_in, fan_out = torch.nn.init._calculate_fan_in_and_fan_out(
                        m.weight.data)
                    bound = 1 / math.sqrt(fan_out)
                    torch.nn.init.normal_(m.bias, -bound, bound)


class LunaBlock(torch.nn.Module):
    def __init__(self, in_channels, conv_channels):
        super().__init__()

        self.conv1 = torch.nn.Conv3d(
            in_channels, conv_channels, kernel_size=3, padding=1, bias=True,)
        self.relu1 = torch.nn.ReLU(inplace=True)
        self.conv2 = torch.nn.Conv3d(
            conv_channels, conv_channels, kernel_size=3, padding=1, bias=True,)
        self.relu2 = torch.nn.ReLU(inplace=True)

        self.maxpool = torch.nn.MaxPool3d(2, 2)

    def forward(self, input_batch):
        block_out = self.conv1(input_batch)
        block_out = self.relu1(block_out)
        block_out = self.conv2(block_out)
        block_out = self.relu2(block_out)

        return self.maxpool(block_out)


class Luna:
    def __init__(self, args: dict) -> None:
        cuda = args.get("cuda")
        if cuda and torch.cuda.is_available():
            self.device_ = "cuda"
        else:
            self.device_ = "cpu"
        print(self.device_)
        self.model_ = LunaModel().to(self.device_)
        self.batch_size_ = args.get("batch_size")
        self.epoch_ = args.get("epoch")
        self.learning_rate_ = args.get("learning_rate")
        self.momentum_ = args.get("momentum")
        self.save_model_ = args.get("save_model")
        # posi_count = 1351
        # nega_count = 549714
        self.weight_ = torch.tensor([0.002451616415486413,
                                    0.997548383584513600], dtype=torch.float32)
        self.loss_per_epoch_ = dict()
        self.positive_accuracy_ = dict()
        self.negative_accuracy_ = dict()

        optimizer_flag = args.get("optimizer")
        if optimizer_flag.casefold() == "sgd":
            self.optimizer_ = torch.optim.SGD(params=self.model_.parameters(),
                                              lr=self.learning_rate_,
                                              momentum=self.momentum_)
        if optimizer_flag.casefold() == "adam":
            self.optimizer_ = torch.optim.Adam(params=self.model_.parameters(),
                                               lr=self.learning_rate_)

        loss_function_flag = args.get("loss_function")
        if loss_function_flag.casefold() == "cross-entropy":
            self.loss_function_ = torch.nn.CrossEntropyLoss(
                reduction="sum", weight=self.weight_).to(self.device_)

        self.train_set_ = LunaDataset(is_eval_set=False, eval_stride=10)
        self.eval_set_ = LunaDataset(is_eval_set=True, eval_stride=10)

        self.train_loader_ = torch.utils.data.dataloader.DataLoader(
            dataset=self.train_set_, batch_size=self.batch_size_, shuffle=False)
        self.eval_loader_ = torch.utils.data.dataloader.DataLoader(
            dataset=self.eval_set_, batch_size=self.batch_size_, shuffle=False)

    def RunLoop(self):
        for epoch in range(1, 1 + self.epoch_):
            self.Train(epoch)
            self.Eval(epoch)
        if self.save_model_:
            pass

    def Train(self, epoch):
        self.model_.train()
        trained = 0
        total = len(self.train_set_)
        for batch in self.train_loader_:
            series_id, data, label, center_irc = batch
            data = data.to(self.device_)
            label = label.to(self.device_)

            self.optimizer_.zero_grad()
            _, prediction = self.model_(data)
            loss = self.loss_function_(prediction, label)
            loss.backward()
            self.optimizer_.step()
            trained += self.batch_size_
            logging.error("epoch:{}/{}, process: {:.2f}%, loss: {}".format(
                epoch, self.epoch_, trained/total * 100, loss))

    def Eval(self, epoch):
        self.model_.eval()

        logging.error("Evalution set")

        negative_count = 0
        positive_count = 0
        correct_neagtive_count = 0
        correct_positive_count = 0

        with torch.no_grad():
            for batch in self.eval_loader_:
                series_id, data, label, center_irc = batch
                data = data.to(self.device_)
                label = label.to(self.device_)

                _, prediction = self.model_(data)
                for l, p in zip(label, prediction):
                    if l[0] > l[1]:
                        negative_count += 1
                        if p[0] > p[1]:
                            correct_neagtive_count += 1
                    else:
                        positive_count += 1
                        if p[0] < p[1]:
                            correct_positive_count += 1
            print("negative_accuracy: {}".format(
                correct_neagtive_count/negative_count))
            print("positive_accuracy: {} ".format(
                correct_positive_count/positive_count))

        logging.error("Train set")
        negative_count = 0
        positive_count = 0
        correct_neagtive_count = 0
        correct_positive_count = 0

        with torch.no_grad():
            for batch in self.train_loader_:
                series_id, data, label, center_irc = batch
                data = data.to(self.device_)
                label = label.to(self.device_)

                _, prediction = self.model_(data)
                for l, p in zip(label, prediction):
                    if l[0] > l[1]:
                        negative_count += 1
                        if p[0] > p[1]:
                            correct_neagtive_count += 1
                    else:
                        positive_count += 1
                        if p[0] < p[1]:
                            correct_positive_count += 1
            print("negative_accuracy: {}".format(
                correct_neagtive_count/negative_count))
            print("positive_accuracy: {} ".format(
                correct_positive_count/positive_count))


def Main(*args, **kwargs):
    args = CreateArgumentParser().parse_args()
    args = vars(args)
    LunaContext.Initialize(args)
    luna = Luna(args)
    luna.RunLoop()


if __name__ == "__main__":
    sys.exit(Main())
